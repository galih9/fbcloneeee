import React,{ Component } from 'React';
import {
    View,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Image,
    ScrollView,
    StatusBar
} from 'react-native';
import {
    Container,
    Header,
    Text,
    Item,
    Input,
    Icon,
    Button,
    Content,
    Body,
    Tab,
    Tabs,
    TabHeading,
    Card,
    CardItem
} from 'native-base';
import {
    Divider,
} from 'react-native-elements';

import styles from './hstyle';
import Ustatus from './Status';
import Story from './Content';
import Post from './Post';
const datapost = require('../data/datapost');


class FeedScreen extends Component{ 

    constructor() {
        super()
        this.state = {
            users : [
                {username : "asd", image : "https://facebook.github.io/react-native/docs/assets/favicon.png",date : "3 hari yang lalu",avatar : "../img/white.png"},
                {username : "jkl", image : "../img/white.png",date : "2 jam yang lalu",avatar : "../img/prof.jpg"},
                {username : "abc", image : "../img/prof.jpg",date : "10-08-2019",avatar : "../img/prof.jpg"},
                {username : "aopsd", image : "../img/blue.png",date : "10-08-2019",avatar : "../img/prof.jpg"},
                {username : "mno", image : "../img/white.png",date : "10-08-2019",avatar : "../img/prof.jpg"},
            ],
            stories : [
                {name : "kamu", storyImg: "../img/white.png"},
                {name : "dia", storyImg: "../img/white.png"},
                {name : "saya", storyImg: "../img/white.png"},
                {name : "zxc", storyImg: "../img/white.png"},
                {name : "qwe", storyImg: "../img/white.png"},
                {name : "tyyu", storyImg: "../img/white.png"}
            ]
        }
    }
    
    componentDidMount(){
        alert('hello');
    }

    render(){
        return(
            <View>
                {/* tab navigation */}
                <View 
                style={styles.tabBar}>
                    <TouchableOpacity 
                    style={styles.tabItem} 
                    onPress={() => this.props.navigation.navigate('Feeds')}>
                        <Image 
                        source={require('../img/icon/news-active.png')} 
                        style={{
                            width:30,
                            height:30
                        }} 
                        />
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem} 
                    onPress={() => this.props.navigation.navigate('Group')}>
                        <Image 
                        source={require('../img/icon/groups-inactive.png')} 
                        style={{
                            width:30,
                            height:30
                        }} 
                        />
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem} 
                    onPress={() => this.props.navigation.navigate('Watch')}>
                        <Image 
                        source={require('../img/icon/watch-inactive.png')} 
                        style={{
                            width:30,
                            height:30
                        }} 
                        />
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem} 
                    onPress={() => this.props.navigation.navigate('Profil')}>
                        <Image 
                        source={require('../img/icon/profile-inactive.png')} 
                        style={{
                            width:30,
                            height:30
                        }} 
                        />
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem} 
                    onPress={() => this.props.navigation.navigate('Notif')}>
                        <Image 
                        source={require('../img/icon/bell-inactive.png')} 
                        style={{
                            width:30,
                            height:30
                        }} 
                        />
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.tabItem} 
                    onPress={() => this.props.navigation.navigate('Menu')}>
                        <Image 
                        source={require('../img/icon/menu-inactive.png')} 
                        style={{
                            width:25,
                            height:25
                        }}
                        />
                    </TouchableOpacity>
                </View>
                    <Divider 
                    style={{
                        backgroundColor:'#9ea2a8',
                        height:1
                    }}
                    />
                {/* Content */}
                <ScrollView>
                    <Ustatus />
                    <Divider 
                        style={{
                            backgroundColor:'#9ea2a8',
                            height:10
                        }}
                    />



                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}  >

                    <View style={{backgroundColor:'#ffffff',width:'100%', height:200}}  >
                        <View style={{flex:1,flexDirection:'row',paddingHorizontal:5,marginVertical:10}} >
                        {/* add own story */}
                        <View style={styles.wrapperImageStory} >
                            <Image style={styles.imageStory} 
                                source={require('../img/prof.jpg')} />
                            <Image style={{resizeMode:'cover',position:'absolute',margin:3,width:34,height:34,borderRadius:100}}
                                source={require('../img/plus.png')} />
                                <Text style={{
                                    color:'black',
                                    position:'absolute',
                                    fontSize:13,
                                    bottom:5,
                                    textAlign:'center',
                                    alignItems:'center',
                                    alignItems:"flex-end"}} >Add to story</Text>
                        </View>

                    {
                        this.state.stories.map((story)=>{
                            return (
                                <Story
                                    name={story.name}
                                    story={story.storyImg}
                                />
                            )
                        })
                    }

                    </View>
                    </View>
                    </ScrollView>




                    <Divider 
                        style={{
                            backgroundColor:'#9ea2a8',
                            height:10
                        }} 
                    />  

                    {
                        this.state.users.map((user)=>{
                            return (
                                <Post
                                    username={user.username}
                                    date={user.date}
                                    image={user.image}
                                    avatar={user.avatar}
                                />
                            )
                        })
                    }
                </ScrollView>
            </View>
            
        )
    }
}
export default FeedScreen
