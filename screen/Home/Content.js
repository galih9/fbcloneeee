import React, { Component } from 'react';
import { Text, TextInput, View ,Button,StyleSheet,Image,ScrollView,StatusBar,TouchableOpacity} from 'react-native';

import styles from './hstyle'

export default class Story extends Component {
  render() {
    return (
      <View style={{flex:1,flexDirection:'row',paddingHorizontal:5,marginVertical:10}}>
        <View style={styles.wrapperImageStory} >
          <Image style={styles.imageStory} 
                  source={require('../img/story.png')}/>
            <View style={styles.wrapperPublicStory} >
                    <Image 
                      style={{resizeMode:'cover',position:'absolute',width:34,height:34,borderRadius:100}}
                      source={{uri: this.props.storyImg}} />
                </View>
            <Text style={styles.namePeopleStory} >
              {this.props.name}
            </Text>
        </View>
      </View>
    )
  }
}