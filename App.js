import React, {Component} from 'react';
import {
   createStackNavigator,
   createAppContainer
} from 'react-navigation';

import IndexScreen from './screen/index'
import LoginScreen from './screen/Login';
import FeedScreen from './screen/Home/Feeds';
import ModalScreen from './screen/Modal';
import GroupScreen from './screen/Groups/Groups';
import ProfilScreen from './screen/Profile/Profile';
import MenuScreen from './screen/Menu/Menu';
import NotifScreen from './screen/Notification/Notif';
import WatchScreen from './screen/Watch/Video';
import PostStatus from './screen/Home/PostStatus';

class App extends Component{
  render(){
    return(
      <AppContainer />

    );
  }

}

const AppNavigator = createStackNavigator({
  
  
  Login: {
    screen: LoginScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },
  Watch: {
    screen: WatchScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },
  Notif: {
    screen: NotifScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },
  Feeds: {
    screen: FeedScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },
  Profil: {
    screen: ProfilScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },
  Group: {
    screen: GroupScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },  
  Menu: {
    screen: MenuScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },
  Index: {
    screen: IndexScreen,
    navigationOptions: ({navigation}) => ({
      header: null,
    })
  },
  Pstatus: {
    screen : PostStatus,
  },
  Modal: {
    screen: ModalScreen,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },

  initialRouteName: 'Feeds'
});

const AppContainer = createAppContainer(AppNavigator)

export default App